# Notehub

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.6.

## Main goals

### Stage 1
- [x] Create project  
- [x] Create gitlab project  
- [x] Connect project  
- [x] Set readme  
- [ ] Set main components and routing  
- [ ] Set main functional and editing  

### Stage 2
- [ ] Buy domain  
- [ ] Buy hosting for all other projects  
- [ ] Connect and set deployment
- [ ] Set autodeployment

### Stage 3
- [ ] Set authorisation
- [ ] Try add secure note send
- [ ] Set meta description for project
- [ ] Set links for project